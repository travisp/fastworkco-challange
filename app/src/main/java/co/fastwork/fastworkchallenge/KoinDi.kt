package co.fastwork.fastworkchallenge

import co.fastwork.fastworkchallenge.core.Api
import co.fastwork.fastworkchallenge.core.FavoriteManager
import co.fastwork.fastworkchallenge.core.MoshiDateAdapter
import co.fastwork.fastworkchallenge.core.SharedPreferenceFavoriteManager
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.dsl.module.Module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


val myKoinModule: Module = org.koin.dsl.module.applicationContext {
    bean {
        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .add(MoshiDateAdapter())
                .build()
        Retrofit.Builder()
                .baseUrl("http://www.mocky.io/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
                .create(Api::class.java)
    }
    bean { SharedPreferenceFavoriteManager(get()) as FavoriteManager }
}