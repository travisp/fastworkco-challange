package co.fastwork.fastworkchallenge.ui.productlist

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.ui.inflate
import kotlinx.android.synthetic.main.item_tag.view.*

class TagAdapter : RecyclerView.Adapter<TagHolder>() {

    var list: List<String> = listOf("Taxonomy1", "Taxonomy2", "Taxonomy3", "Taxonomy4", "Taxonomy5")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagHolder = TagHolder(parent)

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: TagHolder, position: Int) {
        holder.tv.text = list[position]
    }
}

class TagHolder(container: ViewGroup) : RecyclerView.ViewHolder(container.inflate(R.layout.item_tag, false)) {
    val tv: TextView = itemView.tv_title
}