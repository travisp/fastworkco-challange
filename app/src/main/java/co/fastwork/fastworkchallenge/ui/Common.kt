package co.fastwork.fastworkchallenge.ui

import android.support.annotation.LayoutRes
import android.support.annotation.VisibleForTesting
import android.view.LayoutInflater
import android.view.ViewGroup
import co.fastwork.fastworkchallenge.BuildConfig
import com.airbnb.mvrx.BaseMvRxViewModel
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.withState

abstract class AbstractViewModel<S : MvRxState>(initialState: S) : BaseMvRxViewModel<S>(initialState, debugMode = BuildConfig.DEBUG) {
    /**
     * Since there is no instruction from MvRx documentation on how to test it
     * This is how to test ViewModel for the time being
     */
    @VisibleForTesting
    val state: S
        get() = withState(this) { it }

}

fun ViewGroup.inflate(@LayoutRes layout: Int, attachToRoot: Boolean = false) = LayoutInflater.from(context).inflate(layout, this, attachToRoot)