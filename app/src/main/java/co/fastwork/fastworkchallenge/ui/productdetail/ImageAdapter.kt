package co.fastwork.fastworkchallenge.ui.productdetail

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.ui.inflate
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_photo.view.*
import kotlin.properties.Delegates

class ImageAdapter(private val onClick: (String) -> Unit) : RecyclerView.Adapter<ImageHolder>() {

    var list: List<String> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder = ImageHolder(parent, onClick)

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bind(list[position])
    }
}

class ImageHolder(container: ViewGroup, val onClick: (String) -> Unit) : RecyclerView.ViewHolder(container.inflate(R.layout.item_photo)) {
    val iv: ImageView = itemView.iv
    lateinit var image: String

    init {
        itemView.setOnClickListener { onClick(image) }
    }

    fun bind(image: String) {
        this.image = image
        Glide.with(iv).load(image).apply(RequestOptions.centerCropTransform()).into(iv)
    }
}