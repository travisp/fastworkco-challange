package co.fastwork.fastworkchallenge.ui.productdetail

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.ui.productlist.FavoriteProduct
import co.fastwork.fastworkchallenge.ui.productlist.ProductListViewModel
import com.airbnb.mvrx.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.product_detail_fragment.*
import kotlinx.android.synthetic.main.product_detail_fragment.view.*

class ProductDetailFragment : BaseMvRxFragment() {

    val vm: ProductListViewModel by existingViewModel(ProductListViewModel::class) // make fragment leak
    val id: String by lazy { ProductDetailFragmentArgs.fromBundle(arguments).id }
    val adapter: ImageAdapter = ImageAdapter {
        findNavController().navigate(ProductDetailFragmentDirections.actionProductDetailFragmentToFullscreenPhotoFragment(it))
    }
    lateinit var product: FavoriteProduct
    var isDescriptionExpanded: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.product_detail_fragment, container, false).apply {
            rv_photos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rv_photos.adapter = adapter
            val snapHelper = ListenablePagerSnapHelper()
            snapHelper.attachToRecyclerView(rv_photos)
            AutoScroller(lifecycle, rv_photos, snapHelper)
            tv_back.setOnClickListener { findNavController().popBackStack() }
            iv_more.setOnClickListener {}
            tv_fav.setOnClickListener { vm.onFavoriteChange(product.product.objectID, !product.isFavorite) }
            btn_ask.setOnClickListener { showBlankPopup() }
            btn_send_message.setOnClickListener { showBlankPopup() }
            tv_desc.setOnClickListener {
                isDescriptionExpanded = !isDescriptionExpanded
                val lines = if (isDescriptionExpanded) Int.MAX_VALUE else 3
                tv_desc.maxLines = lines
            }
        }
    }

    private fun showBlankPopup() {
        AlertDialog.Builder(context!!)
                .setMessage("May the force be with you")
                .setPositiveButton("OK", null)
                .create()
                .show()
    }

    override fun invalidate() {
        withState(vm) { state ->
            when (state.productAsync) {
                is Uninitialized, is Loading -> {
                }
                is Fail -> {
                }
                is Success -> {
                    product = state.productList.first { it.product.objectID == id }
                    product.populate()
                }
            }
        }
    }

    private fun FavoriteProduct.populate() {
        with(product) {
            tv_title1.text = title
            Glide.with(iv_user_image).load(user.image).apply(RequestOptions.circleCropTransform()).into(iv_user_image)
            tv_user_name.text = user.first_name
            rating_bar.rating = rating.toFloat()
            tv_title2.text = title
            tv_desc.text = description
            adapter.list = photos.map { it.image_medium }
        }
        val icon = if (isFavorite) R.drawable.ic_favorite_grey_500_24dp
        else R.drawable.ic_favorite_border_grey_500_24dp
        tv_fav.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(context!!, icon), null, null)
    }
}