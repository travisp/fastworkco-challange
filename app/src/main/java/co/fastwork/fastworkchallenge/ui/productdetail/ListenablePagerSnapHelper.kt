package co.fastwork.fastworkchallenge.ui.productdetail

import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView

class ListenablePagerSnapHelper : PagerSnapHelper() {

    private val listeners: MutableList<(Int) -> Unit> = mutableListOf()

    fun addListener(listener: (Int) -> Unit) {
        listeners.add(listener)
    }

    fun removeListener(listener: (Int) -> Unit) {
        listeners.remove(listener)
    }

    override fun findTargetSnapPosition(layoutManager: RecyclerView.LayoutManager?, velocityX: Int, velocityY: Int): Int {
        return super.findTargetSnapPosition(layoutManager, velocityX, velocityY).also {
            if (it != RecyclerView.NO_POSITION) {
                for (listener in listeners) {
                    listener(it)
                }
            }
        }
    }
}