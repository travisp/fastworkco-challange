package co.fastwork.fastworkchallenge.ui.productdetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.fastwork.fastworkchallenge.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fullscreen_photo_fragment.view.*

class FullscreenPhotoFragment : Fragment() {

    val uri: String by lazy { FullscreenPhotoFragmentArgs.fromBundle(arguments).uri }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fullscreen_photo_fragment, container, false).apply {
            Glide.with(iv).load(uri).apply(RequestOptions.centerInsideTransform()).into(iv)
        }
    }
}