package co.fastwork.fastworkchallenge.ui.productlist

import android.support.v4.app.FragmentActivity
import co.fastwork.fastworkchallenge.core.Api
import co.fastwork.fastworkchallenge.core.FavoriteManager
import co.fastwork.fastworkchallenge.core.Product
import co.fastwork.fastworkchallenge.ui.AbstractViewModel
import com.airbnb.mvrx.*
import org.koin.android.ext.android.inject

data class ProductListState(
        val productAsync: Async<List<Product>> = Uninitialized,
        val productList: List<FavoriteProduct> = emptyList()
) : MvRxState

class ProductListViewModel(private val api: Api, private val favoriteManager: FavoriteManager) : AbstractViewModel<ProductListState>(ProductListState()) {

    val favoriteListener: (id: String, favorite: Boolean) -> Unit = { id: String, favorite: Boolean ->
        setState { copy(productList = productList.map { FavoriteProduct(it.product, favoriteManager.isFavorite(it.product.objectID)) }) }
    }

    init {
        logStateChanges()
        api.getProduct().execute {
            val list = it()?.map { product -> FavoriteProduct(product, favoriteManager.isFavorite(product.objectID)) }
            copy(productAsync = it, productList = list ?: emptyList())
        }
        favoriteManager.addFavoriteListener(favoriteListener)
    }

    fun onFavoriteChange(id: String, favorite: Boolean) {
        favoriteManager.setFavorite(id, favorite)
    }

    override fun onCleared() {
        super.onCleared()
        favoriteManager.removeFavoriteListener(favoriteListener)
    }

    companion object : MvRxViewModelFactory<ProductListState> {
        @JvmStatic
        override fun create(activity: FragmentActivity, state: ProductListState): BaseMvRxViewModel<ProductListState> {
            val api: Api by activity.inject()
            val favoriteManager: FavoriteManager by activity.inject()
            return ProductListViewModel(api, favoriteManager)
        }
    }
}

data class FavoriteProduct(val product: Product, val isFavorite: Boolean)