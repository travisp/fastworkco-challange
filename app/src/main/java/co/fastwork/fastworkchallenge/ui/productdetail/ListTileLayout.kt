package co.fastwork.fastworkchallenge.ui.productdetail

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import co.fastwork.fastworkchallenge.R
import kotlinx.android.synthetic.main.layout_list_tile.view.*

class ListTileLayout(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    val icon: Drawable?
    val label: String?
    val trailing: String?

    init {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.ListTileLayout, 0, 0)
        try {
            icon = typedArray.getDrawable(R.styleable.ListTileLayout_leading)
            label = typedArray.getString(R.styleable.ListTileLayout_label)
            trailing = typedArray.getString(R.styleable.ListTileLayout_trailing)
        } finally {
            typedArray.recycle()
        }
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        View.inflate(context, R.layout.layout_list_tile, this)
        iv_leading.setImageDrawable(icon)
        tv_label.text = label
        tv_trailing.text = trailing
    }
}