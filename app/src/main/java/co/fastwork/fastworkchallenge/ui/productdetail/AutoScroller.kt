package co.fastwork.fastworkchallenge.ui.productdetail

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class AutoScroller(
        private val lifecycle: Lifecycle,
        private val recyclerView: RecyclerView,
        private val snapHelper: ListenablePagerSnapHelper
) : LifecycleObserver {
    private val interval: Observable<Long> = Observable.interval(3L, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread())
    private var currentIndex: Int = 0
    private var maxIndex: Int = 0
    private val disposable: Disposable
    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            currentIndex = 0
            maxIndex = recyclerView.adapter.itemCount
        }
    }
    private val snapListener: (Int) -> Unit = {
        currentIndex = it
    }

    init {
        lifecycle.addObserver(this)
        recyclerView.adapter.registerAdapterDataObserver(adapterDataObserver)
        snapHelper.addListener(snapListener)
        disposable = interval.subscribe {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                scrollNext()
            }
        }
    }

    private fun scrollNext() {
        val nextIndex = if (currentIndex < maxIndex) currentIndex + 1 else 0
        recyclerView.smoothScrollToPosition(nextIndex)
        currentIndex = nextIndex
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun stop() {
        snapHelper.removeListener(snapListener)
        recyclerView.adapter.unregisterAdapterDataObserver(adapterDataObserver)
        disposable.dispose()
    }
}