package co.fastwork.fastworkchallenge.ui.productlist

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.core.Product
import co.fastwork.fastworkchallenge.ui.inflate
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_product.view.*
import kotlin.properties.Delegates

class ProductAdapter(private val onClick: (FavoriteProduct) -> Unit, private val onFavoriteChange: (Product, Boolean) -> Unit) : RecyclerView.Adapter<ProductHolder>() {

    var list: List<FavoriteProduct> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(container: ViewGroup, position: Int): ProductHolder {
        return ProductHolder(container, onClick, onFavoriteChange)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.bind(list[position])
    }
}

class ProductHolder(container: ViewGroup, private val onClick: (FavoriteProduct) -> Unit, val onFavClick: (Product, Boolean) -> Unit) : RecyclerView.ViewHolder(container.inflate(R.layout.item_product, false)) {

    val ivCover: ImageView = itemView.iv_cover
    val ivUserImage: ImageView = itemView.iv_user_image
    val tvUserName: TextView = itemView.tv_user_name
    val ivFav: ImageView = itemView.iv_fav
    val tvTitle: TextView = itemView.tv_title
    val ratingBar: RatingBar = itemView.rating_bar
    val tvRatingCount: TextView = itemView.tv_rating_count

    lateinit var product: FavoriteProduct

    init {
        itemView.setOnClickListener { onClick(product) }
        ivFav.setOnClickListener { onFavClick(product.product, !product.isFavorite) }
    }

    fun bind(product: FavoriteProduct) {
        this.product = product
        with(product.product) {
            Glide.with(ivCover).load(photos.firstOrNull { it.is_cover_photo }?.image_thumbnail).apply(RequestOptions.centerCropTransform()).into(ivCover)
            Glide.with(ivUserImage).load(user.image).apply(RequestOptions.circleCropTransform()).into(ivUserImage)
            tvUserName.text = user.first_name
            tvTitle.text = title
            ratingBar.numStars = 5
            ratingBar.rating = rating.toFloat()
            tvRatingCount.text = "($purchase)"
            ivFav.setImageResource(
                    if (product.isFavorite) R.drawable.ic_favorite_grey_500_24dp
                    else R.drawable.ic_favorite_border_grey_500_24dp)
        }
    }
}