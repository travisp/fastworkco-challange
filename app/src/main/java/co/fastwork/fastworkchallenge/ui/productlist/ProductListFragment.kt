package co.fastwork.fastworkchallenge.ui.productlist

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.core.Product
import com.airbnb.mvrx.*
import kotlinx.android.synthetic.main.product_list_fragment.*
import kotlinx.android.synthetic.main.product_list_fragment.view.*

class ProductListFragment : BaseMvRxFragment() {

    private val vm: ProductListViewModel by activityViewModel(ProductListViewModel::class)

    private val adapter: ProductAdapter = ProductAdapter(
            onClick = {
                val navigationAction = ProductListFragmentDirections.actionProductListFragmentToProductDetailFragment(it.product.objectID)
                findNavController().navigate(navigationAction)
            },
            onFavoriteChange = { product: Product, b: Boolean ->
                vm.onFavoriteChange(product.objectID, b)
            }
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.product_list_fragment, container, false).apply {
            rv_product_list.adapter = adapter
            rv_tags.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rv_tags.adapter = TagAdapter()
            tv_filter.setOnClickListener { showPopup() }
            rrbg.setOnClickedButtonListener { button, position -> showPopup(button.text) }
        }
    }

    private fun showPopup(message: String = "Live long and prosper") {
        AlertDialog.Builder(context!!)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show()
    }

    override fun invalidate() {
        withState(vm) { state ->
            when (state.productAsync) {
                is Loading, is Uninitialized -> progress_circular.visibility = View.VISIBLE
                is Success -> {
                    progress_circular.visibility = View.GONE
                    rv_product_list.visibility = View.VISIBLE
                    adapter.list = state.productList
                }
                is Fail -> {
                    progress_circular.visibility = View.GONE
                    tv_error.text = state.productAsync.error.message
                }
            }
        }
    }

}
