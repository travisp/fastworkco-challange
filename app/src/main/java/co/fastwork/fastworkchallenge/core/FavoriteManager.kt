package co.fastwork.fastworkchallenge.core

import android.content.Context
import android.content.SharedPreferences

interface FavoriteManager {
    fun setFavorite(id: String, favorite: Boolean)
    fun isFavorite(id: String): Boolean
    fun addFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit)
    fun removeFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit)
}

class SharedPreferenceFavoriteManager(context: Context) : FavoriteManager {

    private val listeners: MutableList<(id: String, favorite: Boolean) -> Unit> = mutableListOf()
    private val sp: SharedPreferences = context.getSharedPreferences("fav", Context.MODE_PRIVATE)

    override fun setFavorite(id: String, favorite: Boolean) {
        sp.edit().putBoolean(id, favorite).apply()
        listeners.forEach { it(id, favorite) }
    }

    override fun isFavorite(id: String): Boolean {
        return sp.getBoolean(id, false)
    }

    override fun addFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit) {
        listeners.add(listener)
    }

    override fun removeFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit) {
        listeners.remove(listener)
    }
}