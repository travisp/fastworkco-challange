package co.fastwork.fastworkchallenge.core

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.SimpleDateFormat
import java.util.*

class MoshiDateAdapter {

    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)

    @FromJson
    fun fromJson(json: String): Date {
        return formatter.parse(json)
    }

    @ToJson
    fun toJson(date: Date): String {
        return formatter.format(date)
    }
}