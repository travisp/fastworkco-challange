package co.fastwork.fastworkchallenge.core

import io.reactivex.Observable
import retrofit2.http.GET

interface Api {
    @GET("v2/5b0275b83000007500cee151")
    fun getProduct(): Observable<List<Product>>
}
