package co.fastwork.fastworkchallenge.core

import java.util.*

data class Product(
        val objectID: String,
        val title: String,
        val description: String,
        val purchase: Int,
        val rating: Int,
        val favorites: Int,
        val photos: List<Photo>,
        val user: User
)

data class Photo(
        val created_at: Date,
        val deleted_at: Date?,
        val id: String,
        val id_mongo: Any?,
        val image_medium: String,
        val image_thumbnail: String,
        val is_cover_photo: Boolean,
        val product_id: String,
        val sort_order: Int,
        val updated_at: String
)

data class User(
        val first_name: String,
        val id: String,
        val image: String,
        val last_online_at: Date,
        val stats: Stats,
        val username: String
)

data class Stats(
        val completion_rate: Double?,
        val conversion_rate: Double?,
        val id: String,
        val response_rate: Double,
        val response_time: Double?,
        val user_id: String
)

