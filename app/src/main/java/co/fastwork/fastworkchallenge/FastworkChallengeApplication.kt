package co.fastwork.fastworkchallenge

import android.app.Application
import org.koin.android.ext.android.startKoin

class FastworkChallengeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(myKoinModule))
    }
}