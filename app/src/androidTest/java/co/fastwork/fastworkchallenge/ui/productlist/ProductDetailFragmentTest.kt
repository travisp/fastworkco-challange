package co.fastwork.fastworkchallenge.ui.productlist

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.ui.MainActivity
import com.agoda.kakao.KTextView
import com.agoda.kakao.Screen
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.standalone.StandAloneContext

class ProductDetailFragmentTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)
    val productListScreen = ProductListScreen()
    val productDetailScreen = ProductDetailScreen()

    @Before
    fun setUp() {
        StandAloneContext.closeKoin() // not sure this is the best way to do it
        StandAloneContext.startKoin(listOf(mockKoinModule))
        rule.launchActivity(Intent())
    }

    @After
    fun tearDown() {
        StandAloneContext.closeKoin()
    }

    @Test
    fun testClickProductListGoToProductDetail() {
        productListScreen {
            rv {
                firstChild<ProductItem> { click() }
            }
        }
        productDetailScreen {
            tv_title1 { hasText("ออกแบบโลโก้เพิ่มความน่าเชื่อถือและดึงดูดลูกค้า!!!") }
        }
    }
}

class ProductDetailScreen : Screen<ProductDetailScreen>() {
    val tv_title1 = KTextView { withId(R.id.tv_title1) }
}