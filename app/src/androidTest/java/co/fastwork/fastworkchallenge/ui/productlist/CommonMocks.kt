package co.fastwork.fastworkchallenge.ui.productlist

import android.support.test.InstrumentationRegistry
import co.fastwork.fastworkchallenge.core.Api
import co.fastwork.fastworkchallenge.core.FavoriteManager
import co.fastwork.fastworkchallenge.core.MoshiDateAdapter
import co.fastwork.fastworkchallenge.core.Product
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Observable
import org.koin.dsl.module.applicationContext


class MockApi : Api {
    override fun getProduct(): Observable<List<Product>> {
        val list = InstrumentationRegistry.getContext().assets.open("mock-product-list.json").use {
            val bytes = it.readBytes()
            val json = String(bytes)
            val moshi = Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .add(MoshiDateAdapter())
                    .build()
            val type = Types.newParameterizedType(List::class.java, Product::class.java)
            moshi.adapter<List<Product>>(type).fromJson(json)!!
        }
        return Observable.just(list)
    }
}

class MockFavoriteManager : FavoriteManager {
    override fun setFavorite(id: String, favorite: Boolean) {

    }

    override fun isFavorite(id: String): Boolean {
        return false
    }

    override fun addFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit) {
    }

    override fun removeFavoriteListener(listener: (id: String, favorite: Boolean) -> Unit) {
    }

}

val mockKoinModule = applicationContext {
    bean { MockApi() as Api }
    bean { MockFavoriteManager() as FavoriteManager }
}