package co.fastwork.fastworkchallenge.ui.productlist

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import android.view.View
import co.fastwork.fastworkchallenge.R
import co.fastwork.fastworkchallenge.core.Api
import co.fastwork.fastworkchallenge.core.FavoriteManager
import co.fastwork.fastworkchallenge.core.MoshiDateAdapter
import co.fastwork.fastworkchallenge.core.Product
import co.fastwork.fastworkchallenge.ui.MainActivity
import com.agoda.kakao.KRecyclerItem
import com.agoda.kakao.KRecyclerView
import com.agoda.kakao.KTextView
import com.agoda.kakao.Screen
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Observable
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.module.applicationContext
import org.koin.standalone.StandAloneContext.closeKoin
import org.koin.standalone.StandAloneContext.startKoin

class ProductListFragmentTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)
    val screen = ProductListScreen()

    @Before
    fun setUp() {
        closeKoin() // not sure this is the best way to do it
        startKoin(listOf(mockKoinModule))
        rule.launchActivity(Intent())
    }

    @After
    fun tearDown() {
        closeKoin()
    }

    @Test
    fun testDisplayMockData() {
        screen {
            tv_fastwork { hasText("Fastwork") }
            rv {
                firstChild<ProductItem> {
                    tv_title { hasText("ออกแบบโลโก้เพิ่มความน่าเชื่อถือและดึงดูดลูกค้า!!!") }
                }
                childAt<ProductItem>(1) {
                    tv_title { hasText("โลลลลโก้ {รับออกแบบโลโก้ทุกสไตล์คร่าบบ}") }
                }
            }
        }
    }
}

class ProductListScreen : Screen<ProductListScreen>() {
    val tv_fastwork = KTextView { withId(R.id.tv_fastwork) }
    val rv = KRecyclerView(
            builder = { withId(R.id.rv_product_list) },
            itemTypeBuilder = {
                itemType(::ProductItem)
            }
    )
}

class ProductItem(parent: Matcher<View>) : KRecyclerItem<ProductItem>(parent) {
    val tv_title: KTextView = KTextView(parent) { withId(R.id.tv_title) }
}