package co.fastwork.fastworkchallenge.ui.productlist

import co.fastwork.fastworkchallenge.core.Api
import co.fastwork.fastworkchallenge.core.Product
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Success
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class ProductListViewModelTest {

    val scheduler: Scheduler = Schedulers.trampoline()

    @Before
    fun setup() {
        RxJavaPlugins.initSingleScheduler { scheduler }
        RxJavaPlugins.initComputationScheduler { scheduler }
        RxJavaPlugins.initNewThreadScheduler { scheduler }
        RxAndroidPlugins.initMainThreadScheduler { scheduler }
    }

    @Test
    fun testCallApiSuccess() {
        // given an api and mock observable
        val list = listOf<Product>()
        val observable = PublishSubject.create<List<Product>>()
        val api: Api = mock { on(it.getProduct()).thenReturn(observable) }

        // when view model is created
        val vm = ProductListViewModel(api, mock())

        // then state is loading
        assertEquals(Loading<List<Product>>(), vm.state.productAsync)

        // when api call finish
        observable.onNext(list)
        observable.onComplete()

        // then state is success
        assertEquals(Success(list), vm.state.productAsync)
    }

}