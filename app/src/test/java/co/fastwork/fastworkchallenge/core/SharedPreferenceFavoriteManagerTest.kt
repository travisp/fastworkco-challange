package co.fastwork.fastworkchallenge.core

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class SharedPreferenceFavoriteManagerTest {

    @Test
    fun testSetFavorite() {
        val id = "MY_ID"
        val favMan = SharedPreferenceFavoriteManager(RuntimeEnvironment.application)
        favMan.setFavorite(id, true)
        Assert.assertTrue(favMan.isFavorite(id))
        favMan.setFavorite(id, false)
        Assert.assertFalse(favMan.isFavorite(id))
    }

    @Test
    fun testListener() {
        var actualId = ""
        var actualFav = false
        val listener: (String, Boolean) -> Unit = { s, b ->
            actualId = s
            actualFav = b
        }
        val id = "MY_ID"
        val favMan = SharedPreferenceFavoriteManager(RuntimeEnvironment.application)
        favMan.addFavoriteListener(listener)
        favMan.setFavorite(id, true)
        Assert.assertEquals("MY_ID", actualId)
        Assert.assertEquals(true, actualFav)
    }
}