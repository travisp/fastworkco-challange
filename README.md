# Mobile Engineering Challenge

This app is build according to a requirement found at https://github.com/fastworkco/mobile-engineering-challenge

You need **Android Studio 3.2 Preview** to open this project. https://developer.android.com/studio/preview/

The solution for the challenge is solely on frontend (mobile, Android) The app is composed of 3 screens

1. Product List
   - filters on top
   - product list below
   - tap to product to go to Product Detail screen
2. Product Detail
   - photos carousel on top
   - details below
   - tap one of the photos to view it fullscreen
3. Fullscreen Photo
   - display a photo in fullscreen

![Product List](images/device-2018-09-03-162415.png) ![Product Detail](images/device-2018-09-03-162516.png) ![Fullscreen Photo](images/device-2018-09-03-162533.png)

## Architectural Design

There are several architectural design patterns out there in the world these day, 
to name a few, MVP, MVVM, CLEAN Architecture. 
The samples of those patterns applied to Android can be found at https://github.com/googlesamples/android-architecture.
Also there is a newly release source code of Google IO 2018 Schedule app here https://github.com/google/iosched which uses CLEAN Architrecture. That is why there is a major hype around "CLEAN Architecture" in Android comminity these day.

CLEAN divides the whole app into 3 layers.

1. Domain layer. This is the business rules.
2. Data layer. This is where the data comes from, abstract away the implementation details for domain layer.
3. Presentation layer. Domain layer gets data from data layer and present to the user here.

However, I believe CLEAN is **not** fit for most of the mobile app (alone) and here is the reason. Say I have an app that fetches something from api, a user can tap on something, the app call another api then display some other thing. You can see that there are no business rules at all in the mobile app, the business rules happens in the backend. So if I were to look at a bigger picture, mobile, frontend, backend as a whole. CLEAN would make sense, backend would act as both domain layer and data layer. Frontend web, Android app and iOS app would be 3 implementations of presentation layer.

For an argument's sake, let's imagine what would have happen if we use CLEAN Architecture in a simple fetch and fire app. 

1. Domain layer. There will be 1 use case for 1 api call doing nothing except delegate the call to datalayer then return the result to presentation layer. These are useless.
2. Data layer. There will be several repositories to abstract several api calls to fetch data. Not useless but sometimes api calls does not fetch/store data but process something like login, then you will have a hard time deciding which repository to put that api call in or if it should be in any "repository" at all.
3. Presentation layer. You have a liberty to choose MVP, MVVM or whatever you want. CLEAN doesn't dictate anything here.

### MvRx

>MvRx (pronounced mavericks) is the Android framework from Airbnb that we use for nearly all product development at Airbnb.

>our goal was not to create yet another architecture pattern for Airbnb, it was to make building products easier, faster, and more fun

MvRx framework/architecture is from Airbnb https://github.com/airbnb/MvRx and **is my architecture of choice for this challenge**. In short, it is like Redux + MVVM.

MvRx is built on top of several components and concepts.
- Kotlin. MvRx uses most of the language feature Kotlin has to offer.
- Android Architecture Components. Android has problems in the framework itself, AAC was release by Google to facilitate them. It is a must.
- RxJava. Functional-reactive programming is a good way to deal with modern apps where there are a lot of interactions, events and api calls everywhere.
- React (conceptually). I find the concept of reactive framework and state easy to understand. Immutable data and pure function makes state mutation a lot easier. This is relatively new in Android world but I have full confidence in it.
- Epoxy (optional but recommended). Epoxy is a library for render view efficiently from Airbnb. This is like React's Reconciliation. Immutable state means we need to re-render all the view to reflect the new state. With Epoxy, the we only need to re-render only the part of view that has changed. Unfortunately, due to a time constraint, I haven't been able to use Epoxy in this app.

### MvRx components

1. State. State is Kotlin immutable data class. State can represent a state of single,a whole app, or a shared state between screen.
2. ViewModel. ViewModel produce new state from current state and business logic or api call.
3. View is just UI component. It represent state visually.
4. Async is super handy. It abstract state of an api call or background processing, make it easy for View to display Loading/Success/Error state.

## Dependency Injection

Dependency injection is the heart of any testable code. [Dagger 2](https://google.github.io/dagger/) is most popular library for the cause.
But since I am a huge fan of Kotlin, I decided that it is time to try out [Koin](https://insert-koin.io/) and it is more pleasurable.

## Testing

There are 4 test classes. 2 unit tests, 2 UI/integration tests

1. Unit - SharedPreferenceFavoriteManagerTest. This test class makes sure when you mark a product favorite, it is favorite.
2. Unit - ProductListViewModelTest. Typically you would test your ViewModel to see if the new state is correct after a function call, api call or some events. I tried but somehow the state is not updated. Since there are no official documentation on how to test it, the best I can do is a fail test.
3. UI/Integration - ProductListFragmentTest. Swap real dependencies with mocks by Koin, supply mock data from json. And check the mock data are display where it supposed to.
4. UI/Integration - ProductDetailFragmentTest. Test clicking an item in ProductListFragment takes you to ProductDetailFragment with correct data shown.

* SharedPreferenceFavoriteManagerTest is a unit test which depend on Android dependency. With a help from [Robolectric](http://robolectric.org/), we can test classes which depend on Android device without actually run the test on device.
* The UI test uses Espresso and a help from [Kakao](https://github.com/agoda-com/Kakao), provide an easy way to create page object and easy view assertion.
* The UI/Integration tests can be test with the real backend just by use the real dependencies but needs to config Espresso to wait for api calls before assertion. [RxIdler](https://github.com/square/RxIdler) is one way to do it.

## Incomplete features

- Localization, all of the hard coded strings should be extracted into strings.xml
- Product detail page, bottom section, no.3 is not fully implemented, the word "ดูเพิ่มเติม" is missing.
- Product detail page, bottom section, no.5 is not fully implemented, the blue words is not blue.
- Lack of error handling in product detail page but consider the page is accessible and uses data from the previous page, the error should not happen.
- Obvious security risk is using http not https, but sure, this is just a challenge.
- Icons are not the exact same ones with the sample.

## Weird stuff and reasoning

- The data from json is in snake_case, it is a norm to map them to a class `Product` with property in camelCase. But I decided to name the properties in snake_case because changing them into camelCase, adds 1 level of indirection to the code. Your brain needs to convert between two cases back and forth. This is debatable.
- There is 1 memory leak due to the use of shared view model under Activity scope in file ProductDetailFragment.kt line 23. Probably because internally it uses an Activity as a lifecycle owner, or I just simply do it wrong. Need to dig deeper into the framework.

## Scalability

- Everything non-ui related lives in `core` package if the app grows bigger you can group them in `core/feature` 
- Everything ui related lives in `ui` package, each screen lives in its own `ui/screen`. More screen - more package.
- If complexity increase, you can borrow use case concept from CLEAN. For example, if 1 business action require 3 api calls, 2 sort operations, 2 local database operations, you could bundle them into 1 class.
- If the project is big enough or complex enough you should consider split it into several small gradle modules. It helps reduce build time and keep each feature isolated. The definition of big enough or complex enough is not clear to me but I can feel when it is time.

## Production-readiness

- Crashlytics and Firebase can be included in the app for monitoring and crash reporting purpose.
- RxJava has a global exception hadler, you can register a handler and log the errors, send to Crashlytics.
- You can build on top of view model and log function calls, send to Crashlytics, combine with crash/error would make debug a lot easier. I have never done this but the idea comes from Redux's logging middleware.